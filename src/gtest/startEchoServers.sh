#/bin/bash

#/bin/bash
if [ $(id -u) -ne 0 ]
  then echo Please run this script as root or using sudo!
  exit
fi

ncat -l 7 --keep-open --exec "/bin/cat" &

ncat -l 7 --keep-open --udp --exec "/bin/cat" &

